# Getting Started with Building React App with Gitlab CI/CD Pipline

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instructions to set up this project successfully
Clone the project to your lacal device or create a fork of the project in your repository

## Scripts to run

In the project directory, you can run:

### `npm install`

Change your working directory into the project folder and run this command
Istall the app dependencies.\

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Push code to Gitlab

At this stage your Gitlab account should be ready
- Create a new prject and push or upload your code to the new repository

## Configuring the CI/CD pipeline

### Open the file named .gitlab-ci.yml

This is the file which has all the configuration for the pipeline.
**View the file for configuration with comments**

### Setting up AWS with Gitlab

At this stage you should have your AWS account ready.

**Things we need from AWS account**
- A programatic IAM user with access S3 Buckets - Set least account privileges
- Create 2 S3 buckets, one for staging environment and the other for production environment
- Configure buckets to be publicly accessible and and for website hosting

### Setting up envirionments and variables

Go into your Project on Gitlab

**Setting up environmets**
- Goto Deployments
- Uder Deployments goto Environments
- Create 2 envrionments, one for staging and the other for production
- *** External link should be the url under Host static website option in your S3 bucket ***
    Configure each environment respectively

**Setting up variables**
- Open settings
- Under settings, goto CI/CD
- Expand variables
    - create a new variable key = AWS_ACCESS_KEY_ID and Value = < Your Aws IAM User >
    - create a new variable key = AWS_SECRET_ACCESS_KEY and Value = < Your Aws IAM User key >
    - create a new variable key = AWS_DEFAULT_REGION and Value = < Your Aws default region (eg. us-east-1) >
    - create a new variable key = AWS_S3_BUCKET and Value = < Your Aws S3 bucket name for staging >
        **At this point your should select staging under variable scope**
    - create a new variable key = AWS_S3_BUCKET and Value = < Your Aws S3 bucket name for production >
        **At this point your should select production under variable scope**

## Additional info

### Disable pushing to main or default branch

- Goto Settings, Repository and Expand Protected branch
- Set Allowed to push to main branch to "NO ONE"

### Configuring a merge request

- Goto settings, General and Expand Merge request
    - Select the option to suit your preference
    - Check the Pipelines must succeed option under 'Merge Checks'
    - Save the changes

## At this point your pipeline should be ready

Create a merge request for every commit and let the pipline run

For more info or clarification contact **jaysechtech@gmail.com** for SUPPORT
